void main(List<String> args) {
  Iterable<int> iterable = [1, 2, 3];
  int value = iterable.elementAt(1);
  print(value);

  const foods = ['Salad', 'Popcorn', 'Toast', 'Lasagne', 'Mamaa'];
  Iterable<String> iterableFoods = foods;
  for (var food in iterableFoods) {
    print(food);
  }

  print('First element is ${foods.first}');
  print('Last element is ${iterableFoods.last}');

  var foundItem1 = foods.firstWhere((element) => element.length > 5);
  print(foundItem1);

  var foundItem2 = foods.firstWhere((element) => element.length > 4);
  print(foundItem2);

  bool predicate(String item) {
    return item.length > 5;
  }

  var foundItem3 = foods.firstWhere(predicate);
  print(foundItem3);

  var foundItem4 =
      foods.firstWhere((item) => item.length > 10, orElse: () => 'None!');
  print(foundItem4);

  var foundItem5 = foods.firstWhere((item) => item.startsWith('M') && item.contains('a'), orElse: () => 'None!');
  print(foundItem5);

  if(foods.any((item) => item.contains('a'))){
    print('At least on item containe "a"');
  }

  if(foods.every((item) => item.length>=5)){
    print('All item have length >= 5');
  }
 
 var numbers = const [1, -2, 3, 42, 0, 4, 5, 6];
 var evenNumbers = numbers.where((number) => number.isEven);
 for (var number in evenNumbers){
   print('$number is even');
 }

 if(evenNumbers.any((number) => number.isNegative)) {
   print('evenNumbers contain negative numbers');
 }

 var largeNumber = evenNumbers.where((number) => number > 1000);
 if(largeNumber.isEmpty){
   print('largeNumber is empty');
 }

 var numbersUntilZero = numbers.takeWhile((number) => number != 0);
 print('Numbers until 0: $numbersUntilZero');

 var numbersStartingZero = numbers.skipWhile((number) => number != 0);
 print('Number starting at 0: $numbersStartingZero');

 var numberByTwo = const [1, -2, 3, 42].map((number) => number *2);
 print('Numbers: $numberByTwo');
}
