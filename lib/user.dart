bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool everyUserOver13(Iterable<User> users) {
  return users.every((user) => user.age > 13);
}

Iterable<User> filterOutUnder21(Iterable<User> users){
  return users.where((user) => user.age >= 21);
}

Iterable<User> filterShortNamed(Iterable<User> users){
  return users.where((user) => user.name.length <= 3);
}

Iterable<String> getNameAndAge(Iterable<User> users){
  return users.map((user) => '${user.name} is ${user.age}');
}
void main() {
  var users = [
    User(name: 'ABCDE', age: 12),
    User(name: 'BCDEF', age: 15),
    User(name: 'CDE', age: 16),
    User(name: 'DEF', age: 21),
    User(name: 'E', age: 25)
  ];

  if (anyUserUnder18(users)) {
    print('Have user under 18');
  }

  if (everyUserOver13(users)) {
    print('Have every user over 18');
  }

  var ageMoreThen21Users = filterOutUnder21(users);
  for (var user in ageMoreThen21Users){
    print(user.toString());
  }

  var shortNamedUsers = filterShortNamed(users);
  for (var user in shortNamedUsers){
    print(user.toString());
  }

  var nameAndAges = getNameAndAge(users);
  for (var user in nameAndAges){
    print(user);
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  @override
  String toString() {
    return '$name, $age';
  }
}
